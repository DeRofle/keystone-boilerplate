import {
    select,
    relationship,
    text,
    timestamp
} from "@keystone-next/keystone/fields"
import { list } from "@keystone-next/keystone"
import { document } from '@keystone-next/fields-document';

import { permissions } from "../access"

export const Post = list({
    access: {
        filter: {
            delete: permissions.canManageContent,
            update: permissions.canManageContent,
        },
    },
    ui: {
        listView: {
            initialColumns: [
                "title",
                "slug",
                "status",
                "content",
            ],
            initialSort: { field: "title", direction: "ASC" },
        },
    },
    fields: {
        title: text({ validation: { isRequired: true } }),
        slug: text({ isIndexed: 'unique', validation: { isRequired: true } }),
        status: select({
          type: 'enum',
          options: [
            { label: 'Draft', value: 'draft' },
            { label: 'Published', value: 'published' },
          ],
        }),
        content: document({
          // We want to have support a fully featured document editor for our
          // authors, so we're enabling all of the formatting abilities and
          // providing 1, 2 or 3 column layouts.
          formatting: true,
          dividers: true,
          links: true,
          layouts: [
            [1, 1],
            [1, 1, 1],
          ],
          // We want to support twitter-style mentions in blogs, so we add an
          // inline relationship which references the `Author` list.
          relationships: {
            mention: {
              kind: 'inline',
              listKey: 'User',
              label: 'Mention', // This will display in the Admin UI toolbar behind the `+` icon
              selection: 'id name', // These fields will be available to the renderer
            },
          },
        }),
        publishDate: timestamp(),
        author: relationship({ ref: 'User.posts', many: false }),
    },
})