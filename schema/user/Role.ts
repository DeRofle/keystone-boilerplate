import {
    checkbox,
    password,
    relationship,
    text,
  } from "@keystone-next/keystone/fields";
  import { list } from "@keystone-next/keystone";
  
  import { permissions } from "../access";
  
  export const Role = list({
    access: {
      filter: {
        delete: permissions.canManageUsers,
        query: permissions.canManageUsers,
        update: permissions.canManageUsers,
      },
    },
    ui: {
      isHidden: (context) => !permissions.canManageUsers(context),
      listView: {
        initialColumns: ["name", "canManageContent", "canManageUsers", "users"],
        initialSort: { field: 'name', direction: 'ASC' },
      },
    },
    fields: {
      name: text(),
      canManageContent: checkbox({ defaultValue: false }),
      canManageUsers: checkbox({ defaultValue: false }),
      users: relationship({ ref: "User.role", many: true, ui: {
        displayMode: "select",
        hideCreate: true,
      } }),
    },
  });
  