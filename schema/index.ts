import { User, Role } from "./user";
import { Post } from './post';
import { Page } from './page';

export const lists = {
    User,
    Role,
    Post,
    Page
}
