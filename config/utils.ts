export const isNumeric = (value: any): boolean => {
    return /^-?\d+$/.test(value)
}
