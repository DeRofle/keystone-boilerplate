import { ServerConfig } from "@keystone-next/keystone/types"
import "../env"
import { isNumeric } from "./utils"

const getPort = (): number => {
    const portEnv = process.env.KEYSTONE_SERVER_PORT

    if (portEnv && isNumeric(portEnv)) {
        return parseInt(portEnv)
    }

    return 3000
}

const getMaxFileSize = (): number => {
  const maxFileSizeEnv = process.env.KEYSTONE_SERVER_MAX_FILE_SIZE

    if (maxFileSizeEnv && isNumeric(maxFileSizeEnv)) {
        return parseInt(maxFileSizeEnv)
    }

    return 200
}

export const server: ServerConfig = {
    cors: {
        credentials: true,
        origin: ["http://localhost:3000/", "http://localhost:3001/"],
    },
    port: getPort(),
    maxFileSize: getMaxFileSize() * 1024 * 1024, // 200 MiB
}
