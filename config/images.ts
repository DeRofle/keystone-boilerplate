import { ImagesConfig } from "@keystone-next/keystone/types"
import "../env"

const getStoragePath = (): string => {
    const storagePathEnv = process.env.KEYSTONE_IMAGES_STORAGE_PATH

    if (storagePathEnv) {
        return storagePathEnv
    }

    return "public/images"
}

const getBaseUrl = (): string => {
    const baseUrlEnv = process.env.KEYSTONE_IMAGES_BASE_URL

    if (baseUrlEnv) {
        return baseUrlEnv
    }

    return "/images"
}

export const images: ImagesConfig = {
    upload: "local",
    local: {
      storagePath: getStoragePath(),
      baseUrl: getBaseUrl(),
    },
  };
  