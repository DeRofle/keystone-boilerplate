import { createAuth } from "@keystone-next/auth";

const { withAuth } = createAuth({
  identityField: "email",
  secretField: "password",
  listKey: "User",
  sessionData: `id name role {
    canManageContent
    canManageUsers
  }`,
  initFirstItem: {
    fields: ["name", "email", "password"],
      itemData: {
        role: {
          create: {
            name: 'Administrator',
            canManageContent: true,
            canManageUsers: true,
          },
        },
      },
  },
});

export { withAuth };
