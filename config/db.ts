import { DatabaseConfig } from "@keystone-next/keystone/types"
import "../env"

const providerNames = ["sqlite", "postgresql"] as const
type ProviderName = typeof providerNames[number]

const getProvider = (): ProviderName => {
    const providerEnv = process.env.KEYSTONE_DB_PROVIDER
    const providerName = providerNames.find(
        (validName) => validName === providerEnv
    )
    if (providerName) {
        return providerName
    }
    return "sqlite"
}

const getUrl = (): string => {
    const provider = getProvider()
    const urlEnv = process.env.KEYSTONE_DB_URL

    if (urlEnv) {
        return urlEnv
    }

    if (provider === "postgresql") {
        throw new Error(
            "Environment variable 'KEYSTONE_DB_URL' not provided, but using 'postgresql' provider. Please provide Environment variable 'KEYSTONE_DB_URL'."
        )
    }

    return "file:./keystone.db"
}

const getUseMigrations = (): boolean =>
    process.env.KEYSTONE_DB_USE_MIGRATIONS === "true"

const getEnableLogging = (): boolean =>
    process.env.KEYSTONE_DB_ENABLE_LOGGING === "true"

export const db: DatabaseConfig = {
    provider: getProvider(),
    url: getUrl(),
    useMigrations: getUseMigrations(),
    enableLogging: getEnableLogging(),
    idField: { kind: "cuid" },
    onConnect: async () => {
        console.log("✨ Database connected")
    },
}
