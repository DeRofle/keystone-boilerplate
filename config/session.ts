import { statelessSessions } from "@keystone-next/keystone/session"
import { isNumeric } from "./utils"

const getSessionSecret = (): string => {
    const sessionSecretEnv = process.env.KEYSTONE_SESSION_SECRET

    if (!sessionSecretEnv) {
        if (process.env.NODE_ENV === "production") {
            throw new Error(
                "Environment variable 'KEYSTONE_SESSION_SECRET' not provided, but running production. Please provide Environment variable 'KEYSTONE_SESSION_SECRET'."
            )
        } else {
            return "539y7hgy4597gy30fj23fg29y43fgr34y567"
        }
    }

    return sessionSecretEnv
}

const getMaxAge = (): number => {
    const maxAgeEnv = process.env.KEYSTONE_SESSION_MAX_AGE;

    if (maxAgeEnv && isNumeric(maxAgeEnv)) {
        return 60 * 60 * 24 * +maxAgeEnv
    }

    return 60 * 60 * 24 * 30; // 30 days
}

const session = statelessSessions({
    maxAge: getMaxAge(),
    secret: getSessionSecret(),
})

export { session }
