import { FilesConfig } from "@keystone-next/keystone/types"
import "../env"

const getStoragePath = (): string => {
    const storagePathEnv = process.env.KEYSTONE_FILES_STORAGE_PATH

    if (storagePathEnv) {
        return storagePathEnv
    }

    return "public/files"
}

const getBaseUrl = (): string => {
    const baseUrlEnv = process.env.KEYSTONE_FILES_BASE_URL

    if (baseUrlEnv) {
        return baseUrlEnv
    }

    return "/files"
}

export const files: FilesConfig = {
    upload: "local",
    local: {
      storagePath: getStoragePath(),
      baseUrl: getBaseUrl(),
    },
  };
  