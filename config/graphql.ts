import { GraphQLConfig } from "@keystone-next/keystone/types"
import "../env"
import { isNumeric } from "./utils"

const getPath = (): string => {
    const pathEnv = process.env.KEYSTONE_GRAPHQL_PATH

    if (pathEnv) {
        return pathEnv
    }

    return "/api/graphql"
}

const getQueryLimits = (): number => {
    const queryLimitsEnv = process.env.KEYSTONE_GRAPHQL_QUERY_LIMITS

    if (queryLimitsEnv && isNumeric(queryLimitsEnv)) {
        return parseInt(queryLimitsEnv)
    }

    return 100
}


export const graphql: GraphQLConfig = {
    debug: process.env.NODE_ENV !== "production",
    path: getPath(),
    queryLimits: { maxTotalResults: getQueryLimits() },
}
