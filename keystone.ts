import { config } from '@keystone-next/keystone';
import { lists } from './schema';
import { db, files, graphql, images, server, session, withAuth, ui } from './config';
import "./env"

export default withAuth(
  config({
    db,
    ui,
    graphql,
    lists,
    server,
    session,
    ...(process.env.KEYSTONE_ENABLE_FILES === "true" && { files }),
    ...(process.env.KEYSTONE_ENABLE_IMAGES === "true" && { images }),
  })
);
