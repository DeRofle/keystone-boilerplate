import React from "react"
import { Page } from "../schema.generated"
import Link from "next/link"

const Layout = ({
    children,
    pages,
}: {
    children: React.ReactNode
    pages: Page[]
}) => {
    const pagesInMenu = pages.filter((page) => page.isPresentInMenu)
    return (
        <>
            <header>
                <ul>
                    {pagesInMenu.map((page) => (
                        <li key={page.id}>
                            <Link href={`/${page.slug}`}>{page.title}</Link>
                        </li>
                    ))}
                </ul>
            </header>
            <div>{children}</div>
            <footer>Footer</footer>
        </>
    )
}

export default Layout
