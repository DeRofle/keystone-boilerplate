import Link from 'next/link';
import React from 'react';
import { User as TUser } from '../schema.generated';
import { apolloClient } from '../services';
import { USERS } from '../services/apollo/queries/users';

export default function Index({ users }: { users: TUser[] }) {
  <h1>Keystone Blog Project - Home</h1>;
  return (
    <ul>
      {users.map(user => (
        <li key={user.id}>
          <h2>
            <Link href={`/author/${user.id}`}>
              <a>{user.name}</a>
            </Link>
          </h2>
          <ul>
            {user.posts && user.posts.map(post => (
              <li key={post.id}>
                <Link href={`/post/${post.slug}`}>
                  <a>{post.title}</a>
                </Link>
              </li>
            ))}
          </ul>
        </li>
      ))}
    </ul>
  );
}

export async function getStaticProps() {
  const { data } = await apolloClient.query({
    query: USERS
  })
  return { props: { users: data.users }, revalidate: 30 };
}