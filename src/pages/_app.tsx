import React from "react"
import { AppProps } from "next/app"
import Layout from "../components/Layout"
import { apolloClient } from "../services"
import { PAGES_LAYOUT } from "../services/apollo/queries/pagesLayout"

function App({ Component, pageProps }: AppProps) {
    return (
        <Layout pages={pageProps.pages}>
            <Component {...pageProps} />
        </Layout>
    )
}

App.getInitialProps = async () => {
    const { data } = await apolloClient.query({
        query: PAGES_LAYOUT,
    })

    return {
        pageProps: {
            pages: data.pages
        }
    }
}

export default App
