import { GetStaticPathsResult, GetStaticPropsContext } from "next"
import Link from "next/link"
import React from "react"
import { DocumentRenderer } from "@keystone-next/document-renderer"
import { Post as TPost, User as TUser } from "../../schema.generated"
import { apolloClient } from "../../services"
import { USERS_ONLY_ID } from "../../services/apollo/queries/usersOnlyId"
import { USER } from "../../services/apollo/queries/user"

export default function Post({ user }: { user: TUser }) {
    return (
        <article>
            <h1>{user.name}</h1>

            <h2>Bio</h2>
            {user.bio?.document && (
                <DocumentRenderer document={user.bio.document} />
            )}
            <h2>Posts</h2>
            {user.posts &&
                user.posts.map((post: TPost) => (
                    <li key={post.id}>
                        <Link href={`/post/${post.slug}`}>
                            <a>{post.title}</a>
                        </Link>
                    </li>
                ))}
        </article>
    )
}

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
    const { data } = await apolloClient.query({
        query: USERS_ONLY_ID,
    })

    return {
        paths: data!.users!.map((user) => ({ params: { id: user.id } })),
        fallback: "blocking",
    }
}

export async function getStaticProps({ params }: GetStaticPropsContext) {
    const { data } = await apolloClient.query({
        query: USER,
        variables: {
            id: params!.id,
        },
    })
    return { props: { user: data.user }, revalidate: 60 }
}
