import { GetStaticPathsResult, GetStaticPropsContext } from 'next';
import React from 'react';
import { DocumentRenderer, DocumentRendererProps } from '@keystone-next/document-renderer';
import { apolloClient } from '../services';
import { PAGES_ONLY_SLUG } from '../services/apollo/queries/pages';
import { PAGE } from '../services/apollo/queries/page';

// By default the DocumentRenderer will render unstyled html elements.
// We're customising how headings are rendered here but you can customise
// any of the renderers that the DocumentRenderer uses.
const renderers: DocumentRendererProps['renderers'] = {
  // Render heading blocks
  block: {
    heading({ level, children, textAlign }) {
      const Comp = `h${level}` as const;
      return <Comp style={{ textAlign, textTransform: 'uppercase' }}>{children}</Comp>;
    },
  },
};

export default function Page({ page }: { page: any }) {
  return (
    <article>
      <h1>{page?.title}</h1>
      {page?.publishDate && (
        <span>
          on <time dateTime={page.publishDate}>{page.publishDate}</time>
        </span>
      )}
      {page?.content?.document && (
        <DocumentRenderer document={page.content.document} renderers={renderers} />
      )}
    </article>
  );
}

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  const { data } = await apolloClient.query({
    query: PAGES_ONLY_SLUG
  })

  return {
    paths: data!.pages!.map((page: any) => ({ params: { slug: page.slug } })),
    fallback: 'blocking',
  };
}

export async function getStaticProps({ params }: GetStaticPropsContext) {
  const { data } = await apolloClient.query({
    query: PAGE,
    variables: {
      slug: params!.slug
    }
  })
  return { props: { page: data.page }, revalidate: 60 };
}