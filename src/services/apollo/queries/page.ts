import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { PageQuery, PageQueryVariables } from "./page.generated";

// We use (hydrateRelationships: true) to ensure we have the data we need
// to render the inline relationships.
const PAGE = gql`
    query Page($slug: String!) {
        page(where: { slug: $slug }) {
            title
            content {
                document(hydrateRelationships: true)
            }
            publishDate
        }
    }
` as TypedDocumentNode<PageQuery, PageQueryVariables>;

export { PAGE }
