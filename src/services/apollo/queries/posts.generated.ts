import * as Types from "../../../schema.generated";

export type PostsOnlySlugQueryVariables = Types.Exact<{ [key: string]: never }>;

export type PostsOnlySlugQuery = {
  __typename?: "Query";
  posts?:
    | Array<{ __typename?: "Post"; slug?: string | null | undefined }>
    | null
    | undefined;
};
