import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { UsersQuery, UsersQueryVariables } from "./users.generated"

const USERS = gql`
    query Users {
        users {
            id
            name
            posts(
                where: { status: { equals: published } }
                orderBy: { publishDate: desc }
            ) {
                id
                slug
                title
            }
        }
    }
` as TypedDocumentNode<UsersQuery, UsersQueryVariables>


export { USERS }
