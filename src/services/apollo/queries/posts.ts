import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { PostsOnlySlugQuery, PostsOnlySlugQueryVariables } from "./posts.generated";

const POSTS_ONLY_SLUG = gql`
    query PostsOnlySlug {
        posts {
            slug
        }
    }
` as TypedDocumentNode<PostsOnlySlugQuery, PostsOnlySlugQueryVariables>;

export { POSTS_ONLY_SLUG }
