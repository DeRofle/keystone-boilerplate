import * as Types from "../../../schema.generated";

export type PageQueryVariables = Types.Exact<{
  slug: Types.Scalars["String"];
}>;

export type PageQuery = {
  __typename?: "Query";
  page?:
    | {
        __typename?: "Page";
        title?: string | null | undefined;
        publishDate?: any | null | undefined;
        content?:
          | { __typename?: "Page_content_Document"; document: any }
          | null
          | undefined;
      }
    | null
    | undefined;
};
