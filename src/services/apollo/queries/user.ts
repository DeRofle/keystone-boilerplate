import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { UserQuery, UserQueryVariables } from "./user.generated"

const USER = gql`
    query User($id: ID!) {
        user(where: { id: $id }) {
          name
          bio {
            document
          }
          posts(where: { status: { equals: published } }, orderBy: { publishDate: desc }) {
            id
            title
            slug
          }
        }
      }
` as TypedDocumentNode<UserQuery, UserQueryVariables>

export { USER  }
