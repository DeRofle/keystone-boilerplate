import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import {
    PagesOnlySlugQuery,
    PagesOnlySlugQueryVariables,
} from "./pages.generated"

const PAGES_ONLY_SLUG = gql`
    query PagesOnlySlug {
        pages {
            slug
        }
    }
` as TypedDocumentNode<PagesOnlySlugQuery, PagesOnlySlugQueryVariables>

export { PAGES_ONLY_SLUG }
