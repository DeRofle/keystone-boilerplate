import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { PostQuery, PostQueryVariables } from "./post.generated"

// We use (hydrateRelationships: true) to ensure we have the data we need
// to render the inline relationships.
const POST = gql`
    query Post($slug: String!) {
        post(where: { slug: $slug }) {
            title
            content {
                document(hydrateRelationships: true)
            }
            publishDate
            author {
                id
                name
            }
        }
    }
` as TypedDocumentNode<PostQuery, PostQueryVariables>

export { POST  }
