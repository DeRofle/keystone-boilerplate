import * as Types from "../../../schema.generated";

export type PagesOnlySlugQueryVariables = Types.Exact<{ [key: string]: never }>;

export type PagesOnlySlugQuery = {
  __typename?: "Query";
  pages?:
    | Array<{ __typename?: "Page"; slug?: string | null | undefined }>
    | null
    | undefined;
};
