import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { PagesLayoutQuery, PagesLayoutQueryVariables } from "./pagesLayout.generated"

const PAGES_LAYOUT = gql`
    query PagesLayout {
        pages {
            id
            title
            slug
            isPresentInMenu
        }
    }
` as TypedDocumentNode<PagesLayoutQuery, PagesLayoutQueryVariables>

export { PAGES_LAYOUT }
