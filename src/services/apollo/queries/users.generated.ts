import * as Types from "../../../schema.generated";

export type UsersQueryVariables = Types.Exact<{ [key: string]: never }>;

export type UsersQuery = {
  __typename?: "Query";
  users?:
    | Array<{
        __typename?: "User";
        id: string;
        name?: string | null | undefined;
        posts?:
          | Array<{
              __typename?: "Post";
              id: string;
              slug?: string | null | undefined;
              title?: string | null | undefined;
            }>
          | null
          | undefined;
      }>
    | null
    | undefined;
};
