import * as Types from "../../../schema.generated";

export type PostQueryVariables = Types.Exact<{
  slug: Types.Scalars["String"];
}>;

export type PostQuery = {
  __typename?: "Query";
  post?:
    | {
        __typename?: "Post";
        title?: string | null | undefined;
        publishDate?: any | null | undefined;
        content?:
          | { __typename?: "Post_content_Document"; document: any }
          | null
          | undefined;
        author?:
          | {
              __typename?: "User";
              id: string;
              name?: string | null | undefined;
            }
          | null
          | undefined;
      }
    | null
    | undefined;
};
