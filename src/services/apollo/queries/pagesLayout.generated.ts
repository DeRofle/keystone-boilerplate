import * as Types from "../../../schema.generated";

export type PagesLayoutQueryVariables = Types.Exact<{ [key: string]: never }>;

export type PagesLayoutQuery = {
  __typename?: "Query";
  pages?:
    | Array<{
        __typename?: "Page";
        id: string;
        title?: string | null | undefined;
        slug?: string | null | undefined;
        isPresentInMenu?: boolean | null | undefined;
      }>
    | null
    | undefined;
};
