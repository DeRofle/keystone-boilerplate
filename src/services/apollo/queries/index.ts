import { POST } from "./post"
import { POSTS_ONLY_SLUG } from "./posts"

export {
    POST,
    POSTS_ONLY_SLUG,
}
