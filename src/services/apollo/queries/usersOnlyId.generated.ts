import * as Types from "../../../schema.generated";

export type UsersOnlyIdQueryVariables = Types.Exact<{ [key: string]: never }>;

export type UsersOnlyIdQuery = {
  __typename?: "Query";
  users?: Array<{ __typename?: "User"; id: string }> | null | undefined;
};
