import * as Types from "../../../schema.generated";

export type UserQueryVariables = Types.Exact<{
  id: Types.Scalars["ID"];
}>;

export type UserQuery = {
  __typename?: "Query";
  user?:
    | {
        __typename?: "User";
        name?: string | null | undefined;
        bio?:
          | { __typename?: "User_bio_Document"; document: any }
          | null
          | undefined;
        posts?:
          | Array<{
              __typename?: "Post";
              id: string;
              title?: string | null | undefined;
              slug?: string | null | undefined;
            }>
          | null
          | undefined;
      }
    | null
    | undefined;
};
