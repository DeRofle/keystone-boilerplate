import { TypedDocumentNode } from "@apollo/client"
import gql from "graphql-tag"
import { UsersOnlyIdQuery, UsersOnlyIdQueryVariables } from "./usersOnlyId.generated"


const USERS_ONLY_ID = gql`
    query UsersOnlyId {
        users {
            id
        }
    }
` as TypedDocumentNode<UsersOnlyIdQuery, UsersOnlyIdQueryVariables>
 
export { USERS_ONLY_ID }