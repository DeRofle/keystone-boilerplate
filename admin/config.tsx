import { AdminConfig } from '@keystone-next/keystone/types';
import { Logo } from './components/Logo';
import { Navigation } from './components/Navigation';

export const components: AdminConfig['components'] = {
  Logo,
  Navigation
};